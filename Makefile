# -*- makefile -*-

# deployment
RUN_AS_USER=yrik
PROCESSES=4
UWSGI:=$(shell type -p uwsgi)

# definitions
APP=prj_name
HOST=127.0.0.1
PORT=8816
CURRPATH=`pwd`

PROJECT_TEST_TARGETS=core

# constants
PYTHONPATH=.:..
PYTHON=python


MANAGE=cd $(APP) && PYTHONPATH=$(PYTHONPATH) DJANGO_SETTINGS_MODULE=$(APP).settings django-admin.py 

# end

run:
	$(MANAGE) runserver $(HOST):$(PORT)

syncdb:
	$(MANAGE) syncdb --noinput
	$(MAKE) manage -e CMD="migrate"

fresh_syncdb: syncdb
	@echo Loading initial fixtures...
	$(MANAGE) loaddata base_initial_data.json
	@echo Done

test:
	TESTING=1 $(MANAGE) test $(TEST_OPTIONS) $(PROJECT_TEST_TARGETS) --verbosity=2

clean:
	@echo Cleaning up *.pyc files
	find . | grep '.pyc$$' | xargs -I {} rm {}

migrate:
ifndef APP_NAME
	@echo Please, specify -e APP_NAME=appname argument
else
	@echo Starting of migration of $(APP_NAME)
	-$(MANAGE) schemamigration $(APP_NAME) --auto
	$(MANAGE) migrate $(APP_NAME)
	@echo Done
endif

init_migrate:
ifndef APP_NAME
	@echo Please, specify -e APP_NAME=appname argument
else
	@echo Starting init migration of $(APP_NAME)
	$(MANAGE) schemamigration $(APP_NAME) --initial
	$(MANAGE) migrate $(APP_NAME)
	@echo Done
endif

shell:
	$(MANAGE) shell

manage:
ifndef CMD
	@echo Please, specify CMD argument to execute Django management command
else
	$(MANAGE)  $(CMD)
endif

help:
	@cat README

# production activation/deactivation
uwsgi_reload:
	-$(MAKE) uwsgi_stop
	$(MAKE) uwsgi

uwsgi:
	$(MANAGE) runfcgi method=threaded host=$(HOST) port=$(PORT) pidfile=$(PID)
	@echo "uwsgi on $(CURRPATH):$(PORT) started"

uwsgi_stop:
	-kill -9 $(shell cat $(PID))
	-rm -f $(PID)
	 
	@echo "uwsgi on $(CURRPATH):$(PORT) stopped"

