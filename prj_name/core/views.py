from random import choice

from django.utils.translation import ugettext as _
from django.forms.formsets import formset_factory
from django.forms.models import modelformset_factory
from django.forms.models import inlineformset_factory
from django.views.decorators.csrf import csrf_exempt
from django.contrib.sites.models import Site
from django.conf import settings

from annoying.decorators import render_to

from core.models import Category, Ingredient, Cocktail, IngredientCount
from core.forms import (CategoryForm, IngredientForm, CocktailForm,
                                            IngredientCountForm)
import plugins


@render_to('get_widget_code.html')
def get_widget_code(request):
    '''Generate widget code from template

    '''

    current_site = Site.objects.get_current()
    current_domain = current_site.domain

    return {'current_domain': current_domain}


@render_to('categories.html')
def categories(request):
    '''Return list of categories'''

    categories = Category.objects.all()

    return {'categories': categories}


@render_to('categories.html')
def category_remove(request, id):
    '''Remove category by id'''

    msg = ''

    try:
        category = Category.objects.get(pk=id)
        category.delete()
        msg = _('Category "%s" was removed') % category
    except Category.DoesNotExist:
        msg = _('Category with id="%" does not exist ') % id
    except e:
        msg = _('Error during removing category: %s') % e

    categories = Category.objects.all()
    return {'categories': categories, 'msg': msg}


@render_to('category.html')
def category_edit(request, id=None):
    '''Edit category by id,
    if id do not provided create new category

    '''

    msg = ''
    try:
        category = Category.objects.get(pk=id)
    except Category.DoesNotExist:
        msg = _('Add new category ')
        category = None

    if request.POST:
        form = CategoryForm(request.POST, instance=category)
        if form.is_valid():
            category = form.save()
            msg = _('Category "%s" successfully saved') % category
        else:
            msg = _('Please, fix errors bellow')
    else:
        form = CategoryForm()

    return {'form': form, 'msg': msg}


@render_to('category.html')
def category_show(request, id=None, to_remove=None):
    '''Show category by id,
    if to_remove passed show option to remove category

    '''

    msg = ''
    try:
        category = Category.objects.get(pk=id)
    except Category.DoesNotExist:
        msg = _('Category not found')
        category = None

    return {'category': category, 'msg': msg, 'to_remove': to_remove}


@render_to('ingredients.html')
def ingredients(request):
    '''List of all ingredients'''

    ingredients = Ingredient.objects.all()

    return {'ingredients': ingredients}


@render_to('ingredients.html')
def ingredient_remove(request, id):
    '''Remove ingredient with given id'''

    msg = ''

    try:
        ingredient = Ingredient.objects.get(pk=id)
        ingredient.delete()
        msg = _('Ingredient "%s" was removed') % ingredient
    except Ingredient.DoesNotExist:
        msg = _('Ingredient with id="%" does not exist ') % id
    except e:
        msg = _('Error during removing ingredient: %s') % e

    ingredients = Ingredient.objects.all()
    return {'ingredients': ingredients, 'msg': msg}


@render_to('ingredient.html')
def ingredient_edit(request, id=None):
    '''Edit ingredient by id,
    if id==None create new ingredient

    '''

    msg = ''
    try:
        ingredient = Ingredient.objects.get(pk=id)
    except Ingredient.DoesNotExist:
        msg = _('Add new ingredient ')
        ingredient = None

    if request.POST:
        form = IngredientForm(request.POST, instance=ingredient)
        if form.is_valid():
            ingredient = form.save()
            msg = _('Ingredient "%s" is successfully saved') % ingredient
        else:
            msg = _('Please, fix errors bellow')
    else:
        form = IngredientForm()

    return {'ingredient': ingredient, 'form': form, 'msg': msg}


@render_to('ingredient.html')
def ingredient_show(request, id=None, to_remove=None):
    '''Show ingredient by id,
    if to_remove passed show option to remove ingredient

    '''

    msg = ''
    try:
        ingredient = Ingredient.objects.get(pk=id)
    except Ingredient.DoesNotExist:
        msg = _('Ingredient not found')
        ingredient = None

    return {'ingredient': ingredient, 'msg': msg, 'to_remove': to_remove}


@render_to('cocktails.html')
def cocktails(request):
    '''List of all cocktails'''

    cocktails = Cocktail.objects.all()

    return {'cocktails': cocktails}


@render_to('cocktails.html')
def cocktail_remove(request, id):
    '''Remove cocktail with given id

    '''

    msg = ''

    try:
        cocktail = Cocktail.objects.get(pk=id)
        cocktail.delete()
        msg = _('Cocktail "%s" was removed') % cocktail
    except Cocktail.DoesNotExist:
        msg = _('Cocktail with id="%" does not exist ') % id
    except e:
        msg = _('Error during removing cocktail: %s') % e

    cocktails = Cocktail.objects.all()
    return {'cocktails': cocktails, 'msg': msg}


@render_to('cocktail.html')
def cocktail_edit(request, id=None):
    '''Edit cocktail by id,
    if id==None create new cocktail

    '''

    msg = ''
    try:
        cocktail = Cocktail.objects.get(pk=id)
    except Cocktail.DoesNotExist:
        msg = _('Add new cocktail ')
        cocktail = None

    IngredientFormSet = modelformset_factory(
        IngredientCount, can_delete=True, exclude=['cocktail'])

    if request.POST:

        form = CocktailForm(request.POST, instance=cocktail)
        ingredients_formset = IngredientFormSet(request.POST)

        if form.is_valid() and  ingredients_formset.is_valid():
            cocktail = form.save()

            # ic_form - means IngredientCount form
            for ic_form in ingredients_formset.cleaned_data:
                if ic_form:

                    if ic_form['DELETE']:
                    # Delete ingr_count
                        ingr_count = ic_form['id']
                        if ingr_count:
                            ingr_count.delete()

                    else:
                        ingr_count = ic_form['id']
                        if ingr_count:
                            ingr_count.ingredient = ic_form['ingredient']
                            ingr_count.count = ic_form['count']
                        else:

                        # Create new ingr_count
                            ingr_count = IngredientCount(
                                cocktail=cocktail,
                                count=ic_form['count'],
                                ingredient=ic_form['ingredient'],
                            )

                        ingr_count.save()

            form = CocktailForm(instance=cocktail)
            ingredients_formset = IngredientFormSet(
                queryset=IngredientCount.objects.filter(cocktail=cocktail))

            msg = _('Cocktail "%s" is successfully saved') % cocktail
        else:
            msg = _('Please, fix errors bellow')
    else:
        form = CocktailForm(instance=cocktail)
        ingredients_formset = IngredientFormSet(
            queryset=IngredientCount.objects.filter(cocktail=cocktail))

    return {'cocktail': cocktail, 'form': form, 'msg': msg,
                'ingredients_formset': ingredients_formset}


@render_to('cocktail.html')
def cocktail_show(request, id=None):
    '''Show selected cocktail

    '''

    msg = ''
    try:
        cocktail = Cocktail.objects.get(pk=id)
    except Cocktail.DoesNotExist:
        msg = _('Category not found')
        cocktail = None

    return {'cocktail': cocktail, 'msg': msg}


@csrf_exempt
@render_to("widget.html")
def get_widget(request):
    '''Pass data from selected plugin to template

    '''

    plugin_name = choice(settings.COCKTAIL_PLUGINS)

    try:
        plugin = getattr(plugins, plugin_name)
        return {'data': plugin().data}
    except Exception, e:
        return {'error': str(e)}
