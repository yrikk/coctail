from django.forms import ModelForm
from django.forms.widgets import HiddenInput

from core.models import (Category, Ingredient, Cocktail,
                                        IngredientCount)


class CategoryForm(ModelForm):
    class Meta:
        model = Category


class IngredientCountForm(ModelForm):
    class Meta:
        model = IngredientCount
        widgets = {
            'cocktail': HiddenInput(),
        }


class IngredientForm(ModelForm):
    class Meta:
        model = Ingredient


class CocktailForm(ModelForm):
    class Meta:
        model = Cocktail
        exclude = ('ingredients',)
