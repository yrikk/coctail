import urllib2
from random import choice

from BeautifulSoup import BeautifulSoup

from core.models import Cocktail

'''To add another plugin add class that has data property
that looks like:

self.data = {
    'title': 'cocktail title',
    'description': 'Cocktail description',
    'ingredients': ['Ingredient 1', 'Ingredient 2'],
    'source': "www.1001cocktails.com",
}

Also you must place plugin(class) name to
settings.COCKTAIL_PLUGINS
if you want to enable this plugin

'''


class Cocktails1001(object):
    '''Get random coktail from
    www.1001cocktails.com

    '''

    data = {}

    def __init__(self):
        self.data = {
            'title': '',
            'description': '',
            'ingredients': '',
            'source': "www.1001cocktails.com",
            'url': 'http://www.1001cocktails.com/recipes/'\
                'cocktails/recipe_cocktail.php?recette_cocktail=00'
        }
        self.get_random_cocktail()

    def get_random_cocktail(self):
        '''Populate data from remote server'''

        html = urllib2.urlopen(self.data['url']).read()
        soup = BeautifulSoup(html, convertEntities=BeautifulSoup.HTML_ENTITIES)
        self.data['title'] = soup.find("h1").string
        self.data['description'] = urllib2.unquote(soup.findAll(
                        attrs={"class": "normal2"})[3].p.contents[0])

        self.data['ingredients'] = [el.getText() for el in
            soup.find("span", attrs={"class": "important2gras"}
                                       ).nextSibling.nextSibling.findAll("td")]


class Internal(object):
    '''Get random coktail from internal DB

    '''

    data = {}

    def __init__(self):
        self.data = {
            'title': '',
            'description': '',
            'ingredients': '',
            'source': 'internal'
        }
        self.get_random_cocktail()

    def get_random_cocktail(self):
        '''Populate data from remote server

        '''

        coktail = choice(Cocktail.objects.all())
        self.data['title'] = coktail.title
        self.data['description'] = coktail.description
        ingredients = ["%s %s" % (x.count, x.ingredient.title)
                            for x in coktail.cocktail_counts.all()]
        self.data['ingredients'] = ingredients
