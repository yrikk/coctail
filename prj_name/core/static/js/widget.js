
function get_cocktail(){
    // pass {} to force POST request
    $('#cocktail_widget').load(cocktail_server + '/api/get_widget/', {}, callback)
}

function callback(responseText){
    $('#show_more_cocktails').click(function(){
        get_cocktail()
    }) 
}

get_cocktail()
