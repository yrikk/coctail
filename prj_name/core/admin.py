from django.contrib import admin

from core.models import (Category, Ingredient,
                    Cocktail, IngredientCount)


class IngredientCountInline(admin.TabularInline):
    model = IngredientCount
    extra = 1


class CocktailAdmin(admin.ModelAdmin):
    inlines = (IngredientCountInline,)


admin.site.register(Category)
admin.site.register(Ingredient)
admin.site.register(Cocktail, CocktailAdmin)
