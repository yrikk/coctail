from django.db import models


class Category(models.Model):
    title = models.CharField(max_length=255)

    def __unicode__(self):
        return '%s' % self.title


class Ingredient(models.Model):
    title = models.CharField(max_length=255)

    def __unicode__(self):
        return '%s' % self.title


class IngredientCount(models.Model):
    '''Additional model to store amount of
    selected ingredient

    '''

    ingredient = models.ForeignKey(Ingredient,
                        related_name='ingredient_counts')
    cocktail = models.ForeignKey('core.Cocktail',
                        related_name='cocktail_counts')
    count = models.CharField(null=True, blank=True, max_length=255)

    def __unicode__(self):
        return '%s:%s %s' % (self.cocktail, self.ingredient, self.count)


class Cocktail(models.Model):
    title = models.CharField(max_length=255)
    description = models.TextField(null=True, blank=True)
    category = models.ForeignKey(Category)
    ingredients = models.ManyToManyField(Ingredient, through=IngredientCount)

    def __unicode__(self):
        return '%s' % self.title
