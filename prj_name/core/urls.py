from django.conf.urls.defaults import patterns, include, url
from django.views.generic.simple import direct_to_template

from core.views import (
     categories, category_edit, category_remove, category_show,
     ingredients, ingredient_edit, ingredient_remove, ingredient_show,
          cocktails, cocktail_edit, cocktail_remove, get_widget,
          cocktail_show, get_widget_code)


urlpatterns = patterns('',
    url(r'^$', direct_to_template, {'template': 'index.html'}),
    url(r'^get_widget_code/$', get_widget_code,
                                    name='get_widget_code'),

    url(r'^ingredients/$', ingredients, name='ingredients'),
    url(r'^ingredient_edit/(?P<id>\d+)/$', ingredient_edit,
                                        name='ingredient_edit'),
    url(r'^ingredient_show/(?P<id>\d+)/$', ingredient_show,
                                        name='ingredient_show'),
    url(r'^ingredient_show/(?P<id>\d+)/(?P<to_remove>1)$',
                ingredient_show, name='ingredient_show_remove'),
    url(r'^ingredient_add/$', ingredient_edit,
                                        name='ingredient_add'),
    url(r'^ingredient_remove/(?P<id>\d+)/$', ingredient_remove,
                                        name='ingredient_remove'),


    url(r'^categories/$', categories, name='categories'),
    url(r'^category_edit/(?P<id>\d+)/$', category_edit,
                                        name='category_edit'),
    url(r'^category_show/(?P<id>\d+)/$', category_show,
                                        name='category_show'),
    url(r'^category_show/(?P<id>\d+)/(?P<to_remove>1)/$', category_show,
                                        name='category_show_remove'),
    url(r'^category_add/$', category_edit,
                                        name='category_add'),
    url(r'^category_remove/(?P<id>\d+)/$', category_remove,
                                        name='category_remove'),


    url(r'^cocktails/$', cocktails, name='cocktails'),
    url(r'^cocktail_edit/(?P<id>\d+)/$', cocktail_edit,
                                        name='cocktail_edit'),
    url(r'^cocktail_show/(?P<id>\d+)/$', cocktail_show,
                                        name='cocktail_show'),
    url(r'^cocktail_add/$', cocktail_edit,
                                        name='cocktail_add'),
    url(r'^cocktail_remove/(?P<id>\d+)/$', cocktail_remove,
                                        name='cocktail_remove'),

    url(r'^api/get_widget/$', get_widget,
                                        name='get_widget'),


)
