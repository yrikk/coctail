from random import choice

from django.test import TestCase
from django.core.urlresolvers import reverse

from core.models import Category, Ingredient, Cocktail


class WidgetApiTestCase(TestCase):

    def test_get_widget(self):
        url = reverse('get_widget')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertNotEqual(len(response.context['data']), 0)


class CocktailTestCase(TestCase):

    def test_list_presentation(self):
        url = reverse('cocktails')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        cocktails = Cocktail.objects.all()
        self.assertEqual(len(response.context['cocktails']),
                                                    len(cocktails))
        test_cocktail = choice(cocktails)
        self.assertContains(response, str(test_cocktail))

    def test_add(self):
        url = reverse('cocktail_add')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        title = 'test_cocktail'
        data = {
            'title': title,
             'category': 1,
             'description': 'desc',
            'form-TOTAL_FORMS': 1,
            'form-INITIAL_FORMS': 0,
            'form-MAX_NUM_FORMS': 20,
            'form-0-ingredient': 1,
            'form-0-count': '1/2 oz'
            }
        response = self.client.post(url, data)

        new_cocktail = Cocktail.objects.order_by('-id').all()[0]
        self.assertEqual(new_cocktail.title, title)
        self.assertEqual(new_cocktail.ingredients.count(), 1)

    def test_edit(self):
        cocktail = choice(Cocktail.objects.all())
        url = reverse('cocktail_edit', kwargs={'id': cocktail.id})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        title = 'new_test_cocktail'
        data = {
            'title': title,
             'category': 1,
             'description': 'desc',
            'form-TOTAL_FORMS': 1,
            'form-INITIAL_FORMS': 0,
            'form-MAX_NUM_FORMS': 20,
            }

        response = self.client.post(url, data)

        cocktail = Cocktail.objects.get(pk=cocktail.id)
        self.assertEqual(cocktail.title, title)


class IngredientTestCase(TestCase):

    def test_list_presentation(self):
        url = reverse('ingredients')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        ingredients = Ingredient.objects.all()
        self.assertEqual(len(response.context['ingredients']),
                                                    len(ingredients))
        test_ingredient = choice(ingredients)
        self.assertContains(response, str(test_ingredient))

    def test_add(self):
        url = reverse('ingredient_add')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        title = 'test_ingredient'
        data = {'title': title}
        response = self.client.post(url, data)

        new_ingredient = Ingredient.objects.order_by('-id').all()[0]
        self.assertEqual(new_ingredient.title, title)

    def test_edit(self):
        ingredient = choice(Ingredient.objects.all())
        url = reverse('ingredient_edit', kwargs={'id': ingredient.id})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        title = 'new_test_ingredient'
        data = {'title': title}
        response = self.client.post(url, data)

        ingredient = Ingredient.objects.get(pk=ingredient.id)
        self.assertEqual(ingredient.title, title)


class CategoryTestCase(TestCase):

    def test_list_presentation(self):
        url = reverse('categories')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        categories = Category.objects.all()
        self.assertEqual(len(response.context['categories']),
                                                    len(categories))
        test_category = choice(categories)
        self.assertContains(response, str(test_category))

    def test_add(self):
        url = reverse('category_add')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        title = 'test_category'
        data = {'title': title}
        response = self.client.post(url, data)

        new_category = Category.objects.order_by('-id').all()[0]
        self.assertEqual(new_category.title, title)

    def test_edit(self):
        category = choice(Category.objects.all())
        url = reverse('category_edit', kwargs={'id': category.id})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        title = 'new_test_category'
        data = {'title': title}
        response = self.client.post(url, data)

        category = Category.objects.get(pk=category.id)
        self.assertEqual(category.title, title)
